An mrunner kickstart pack:

Get the following values from up.neptune.ml. Click help and then quickstart.

```
export NEPTUNE_API_TOKEN = ...
export PROJECT_QUALIFIED_NAME = ...
```
Run basic setup
```
basic_setup.sh <your plgrid login> <grant which you have access to>
```
For example
```
basic_setup.sh plgloss planningrl
```

Run experiments
```
run_examples.sh
```